---
layout: markdown_page
title: Financial Wrap-up
category: Internal
---

Once the SOW items have been delivered in full and the project sign off has been received or we have passive acceptance based off of the language in the SOW:

1. Go into the SalesForce opportunity and scroll to the Sertifi EContracts
2. Select the file where the Project Sign off document was sent
3. Copy the URL and then go back (hit the back button) to the main opportunity page that you were just on
4. At the top of the page, in the post freeform box, chatter Wilson Lau (@wilsonlau) and add a note outlining if we got a signed Project Sign Off or if we have passive acceptance and then copy in the URL. Hit share

