---
layout: markdown_page
title: "Section Direction - <SECTION-NAME>"
---

- TOC
{:toc}

## <SECTION-NAME> Overview
<!-- Provide a general overview of the section and what is covered within it.  Include details on our current market share (if available), the total addressable market (TAM), our competitive position, and high level feedback from customers on current features -->

## Challenges
<!-- What are our constraints? (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->

## 3 Year Strategy
<!-- Where will the product be in 3 years? How will the customer's life/workflow be different in 3 years as a result of our product? -->

## Themes
<!-- Introduce themes that will drive product investment over the next 1-2 years. -->

## One Year Plan
<!-- Describe key projects and/or features planned over the next year that support the themes.  Also highlight what we will not be doing in the next year -->

## Stages and Categories
<!-- Provide brief descriptions of stage + category direction, along with links to supporting direction pages -->

## What's Next
<!-- Provide links to issues for the next three milestones -->


<%= direction %>